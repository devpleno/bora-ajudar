import React, { Component } from 'react'
import { auth } from './base'
import { Redirect, Route, Switch } from 'react-router-dom'

import AdminCampanhas from './AdminCampanhas'
import AdminEditarCampanha from './AdminEditarCampanha'

const AdminHome = props => <p>Seja bem vindo</p>

class Admin extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isAuthing: true,
            isLoggedIn: false,
            user: null
        }
    }

    componentDidMount() {

        auth.onAuthStateChanged(user => {
            this.setState({
                isAuthing: false,
                isLoggedIn: !!user,
                user: user
            })
        })

    }

    render() {

        if(this.state.isAuthing) {
            return <p>Aguarde...</p>
        }

        if(!this.state.isLoggedIn) {
            return <Redirect to="/login" />
        }

        return (
            <div className='card'>
                <h1>Painel do Admin</h1>

                <Switch>
                    <Route exact path='/' component={AdminHome} />
                    <Route path={this.props.match.url + '/campanhas/:id'} component={AdminEditarCampanha} />
                    <Route path={this.props.match.url + '/campanhas'} component={AdminCampanhas} />
                </Switch>
            </div>
        )
    }
}

export default Admin