import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import base from './base'

import Header from './Header'
import Footer from './Footer'
import Home from './Home'
import About from './About'
import Contact from './Contact'
import Campaign from './Campaign'
import Erro404 from './Erro404'

import Login from './Login'
import Admin from './Admin'

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      contador: 1
    }
  }

  componentDidMount() {
    base.syncState('contador', {
      context: this,
      state: 'contador',
      asArray: false
    })
  }

  render() {
    return (
      <Router>
        <div>
          <Header />

          <h1>{this.state.contador}</h1>

          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/sobre' component={About} />
            <Route path='/contato' component={Contact} />
            <Route path='/campanhas' component={Campaign} />
            <Route path='/login' component={Login} />
            <Route path='/admin' component={Admin} />
            
            <Route component={Erro404} />
          </Switch>

          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
