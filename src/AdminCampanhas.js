import React from 'react'
import base from './base'
import {Link} from 'react-router-dom'

class AdminCampanhas extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            campanhas: {}
        }

        this.renderCampanha = this.renderCampanha.bind(this)
        this.removeCampanha = this.removeCampanha.bind(this)
        this.editarCampanha = this.editarCampanha.bind(this)
        this.handleSave = this.handleSave.bind(this)
    }

    componentDidMount() {
        base.syncState('campanhas', {
            context: this,
            state: 'campanhas',
            isArray: false
        })
    }

    editarCampanha(key) {
    }

    removeCampanha(key) {
        // const {[key]:undefined, ...newCampanhas} = this.state.campanhas

        // this.setState({
        //     campanhas: newCampanhas
        // })

        base.remove('campanhas/' + key)
    }

    handleSave() {
        const titulo = this.titulo.value
        const descricao = this.descricao.value
        const subtitulo = this.subtitulo.value
        
        const tipo = this.state.tipo
        
        const meta = this.state.tipo === 'doacao' ? this.meta.value : null
        const doado = this.state.tipo === 'doacao' ? this.doado.value : null

        const comoDoar = this.state.tipo === 'produtos' ? this.comoDoar.value : null

        base.push('campanhas', {
            data: { titulo, descricao, subtitulo, meta, doado, comoDoar, tipo },
            then: err => {

                if (!err) {
                    this.titulo.value = ''
                    this.descricao.value = ''
                    this.subtitulo.value = ''

                    if (tipo === 'doacao') {
                        this.meta.value = ''
                        this.doado.value = ''
                    } else {
                        this.comoDoar.value = ''
                    }

                     this.setState({ tipo: '' })
                }

            }
        })
    }

    renderCampanha(campanha, key, i) {
        return (
            <li key={"key-" + i}>
                {campanha.titulo}

                <Link to={'/admin/campanhas/' + key}>Editar</Link>
                <button onClick={() => this.removeCampanha(key)}>Excluir</button>
            </li>
        )
    }

    render() {
        return (
            <div>
                <h1>Campanhas</h1>

                <h2>Nova campanha</h2>
                Titulo: <input type='text' ref={ref => this.titulo = ref} /><br />
                SubTitulo: <input type='text' ref={ref => this.subtitulo = ref} /><br />
                Descrição: <textarea ref={ref => this.descricao = ref} ></textarea> <br />
                Tipo:
                    <input type='radio' onClick={() => this.setState({ tipo: 'doacao' })} name='tipo' checked={this.state.tipo === 'doacao'} /> Doação
                    <input type='radio' onClick={() => this.setState({ tipo: 'produtos' })} name='tipo' checked={this.state.tipo === 'produtos'} /> Produtos
                    <br />

                {this.state.tipo === 'doacao' &&
                    <div>
                        <h4>Doação</h4>
                        Meta: <input type='text' ref={ref => this.meta = ref} />
                        Já doado: <input type='text' ref={ref => this.doado = ref} defaultValue={0} />
                        <br />
                    </div>
                }

                {this.state.tipo === 'produtos' &&
                    <div>
                        <h4>Produtos</h4>
                        Como doar: <input type='text' ref={ref => this.comoDoar = ref} />
                        <br />
                    </div>
                }

                <button onClick={this.handleSave}>Salvar</button>

                <ul>
                    {Object.keys(this.state.campanhas).map((key, i) => {
                        return this.renderCampanha(this.state.campanhas[key], key, i)
                    }
                    )}
                </ul>
            </div>
        )
    }
}

export default AdminCampanhas