import React from 'react'
import base from './base'
import {Redirect} from 'react-router-dom'

class AdminEditarCampanha extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            campanha: {},
            isLoading: true,
            saved: false
        }

        this.handleSave = this.handleSave.bind(this)
    }

    componentDidMount() {
        const id = this.props.match.params['id']

        base.fetch('campanhas/' + id, {
            context: this,
            isArray: false,
            then(ret) {
                this.setState({ campanha: ret, isLoading: false, tipo: ret['tipo'] })
            }
        })
    }

    handleSave() {
        const id = this.props.match.params['id']
        const titulo = this.titulo.value
        const descricao = this.descricao.value
        const subtitulo = this.subtitulo.value

        const tipo = this.state.tipo

        const meta = this.state.tipo === 'doacao' ? this.meta.value : null
        const doado = this.state.tipo === 'doacao' ? this.doado.value : null

        const comoDoar = this.state.tipo === 'produtos' ? this.comoDoar.value : null

        base.update('campanhas/' + id, {
            data: { titulo, descricao, subtitulo, meta, doado, comoDoar, tipo },
            then: err => {
                if(!err) {
                    this.setState({saved: true})
                }
            }
        })
    }

    render() {
        if (this.state.saved) {
            return <Redirect to={'/admin/campanhas'}></Redirect>
        }

        if (this.state.isLoading) {
            return <div>Carregando...</div>
        }

        return (
            <div>
                <h1>Editar campanha</h1>

                Titulo: <input type='text' ref={ref => this.titulo = ref} defaultValue={this.state.campanha.titulo} /><br />
                SubTitulo: <input type='text' ref={ref => this.subtitulo = ref} defaultValue={this.state.campanha.subtitulo} /><br />
                Descrição: <textarea ref={ref => this.descricao = ref} defaultValue={this.state.campanha.descricao} ></textarea> <br />
                Tipo:
                    <input type='radio' defaultChecked={this.state.campanha.tipo == 'doacao'} onClick={() => this.setState({ tipo: 'doacao' })} name='tipo' /> Doação
                    <input type='radio' defaultChecked={this.state.campanha.tipo == 'produtos'} onClick={() => this.setState({ tipo: 'produtos' })} name='tipo' /> Produtos
                    <br />

                {(this.state.tipo === 'doacao' || this.state.campanha.tipo == 'doacao') &&
                    <div>
                        <h4>Doação</h4>
                        Meta: <input type='text' ref={ref => this.meta = ref} defaultValue={this.state.campanha.meta} />
                        Já doado: <input type='text' ref={ref => this.doado = ref} defaultValue={0} />
                        <br />
                    </div>
                }

                {(this.state.tipo === 'produtos' || this.state.campanha.tipo == 'produtos') &&
                    <div>
                        <h4>Produtos</h4>
                        Como doar: <input type='text' ref={ref => this.comoDoar = ref} defaultValue={this.state.campanha.comoDoar} />
                        <br />
                    </div>
                }

                <button onClick={this.handleSave}>Salvar</button>

            </div>
        )
    }
}

export default AdminEditarCampanha