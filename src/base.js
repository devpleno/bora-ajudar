import Rebase from 're-base'
import firebase from 'firebase'

const config = {
    apiKey: 'AIzaSyAOSLiPeXS9Zysmyy-gaMbcUw0Z3nam0Lc',
    authDomain: 'bora-ajudar-1582.firebaseapp.com',
    databaseURL: 'https://bora-ajudar-1582.firebaseio.com',
    projectId: 'bora-ajudar-1582',
    storageBucket: 'bora-ajudar-1582.appspot.com',
    messagingSenderId: '690229278706'
};

const app = firebase.initializeApp(config);
const base = Rebase.createClass(app.database())

export const auth = firebase.auth();
export default base