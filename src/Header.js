import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Header extends Component {
    render() {
        return (
            <div>
                <h1 className='site-heading text-center text-white d-none d-lg-block'>
                    <img src='img/logo.png' alt='' />
                </h1>

                <nav className='navbar navbar-expand-lg navbar-dark py-lg-4' id='mainNav'>
                    <div className='container'>
                        <a className='navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none' href='/'>Asilo Melhor Idade</a>
                        <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
                            <span className='navbar-toggler-icon'></span>
                        </button>
                        <div className='collapse navbar-collapse' id='navbarResponsive'>
                            <ul className='navbar-nav mx-auto'>
                                <li className='nav-item active px-lg-4'>
                                    <Link className='nav-link text-uppercase text-expanded' to='/'>Início
                                        <span className='sr-only'>(current)</span>
                                    </Link>
                                </li>
                                <li className='nav-item px-lg-4'>
                                    <Link className='nav-link text-uppercase text-expanded' to='/sobre'>Sobre</Link>
                                </li>
                                <li className='nav-item px-lg-4'>
                                    <Link className='nav-link text-uppercase text-expanded' to='/campanhas'>Campanhas</Link>
                                </li>
                                <li className='nav-item px-lg-4'>
                                    <Link className='nav-link text-uppercase text-expanded' to='/contato'>Contato</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}

export default Header