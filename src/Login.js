import React, { Component } from 'react'
import { auth } from './base'
import { Redirect } from 'react-router-dom'

class Login extends Component {

    constructor(props) {
        super(props)

        this.email = null;
        this.password = null;

        this.validarLogin = this.validarLogin.bind(this)

        this.state = {
            isLoggedIn: false,
            error: false,
            isLogging: false
        }
    }

    validarLogin() {
        this.setState({ error: false, isLogging: true })

        auth.signInWithEmailAndPassword(this.email.value, this.password.value).then((user) => {
            this.setState({ isLoggedIn: true })

        }).catch((err) => {
            this.setState({ error: true, isLogging: false })
            console.log('erro', err)
        })
    }

    render() {
        if (this.state.isLoggedIn) {
            return <Redirect to='/admin' />
        }
        
        return (
            <div>
                <input type='email' ref={ref => this.email = ref} />
                <input type='password' ref={ref => this.password = ref} />
                <button disabled={this.state.isLogging} onClick={() => this.validarLogin()}>Entrar</button>

                {this.state.error &&
                    <p>E-mail ou senha invalidos</p>
                }
            </div>
        )
    }
}

export default Login