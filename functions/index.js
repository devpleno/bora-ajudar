const functions = require('firebase-functions');
const admin = require('firebase-admin')

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()

app.use(cors())

admin.initializeApp(functions.config().firebase)

// pagseguro
const request = require('request-promise')
const parse = require('xml2js').parseString
const email = 'tuliofaria@servidor.com'
const token = 'C10698F02E464563BF287D43EC66A6D5'

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (req, res) => {
    res.send('Bora ajudar Server')
})

app.post('/donate', (req, res) => {

    request({
        uri: 'https://ws.pagseguro.uol.com.br/v2/checkout',
        method: 'POST',
        form: {
            token: token,
            email: email,
            currency: 'BRL',
            itemId1: req.body.campanha,
            itemDescription1: 'Doação',
            itemQuantity: '1',
            itemAmount1: req.body.valor
        },
        headers: {
            'Content-type': 'application/x-www-urlencoded; charset=UTF-8'
        }
    }).then(data => {
        parse(data, (err, json) => {
            res.send({
                url: 'https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' + json.checkout.code[0]
            })
        })
    })

})

// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

app.post('/webhook', (req, res) => {

    //const notificationCode = '16DAD7-F881F581F58B-F004753FA1D8-578837'
    const notificationCode = req.body.notificationCode

    request('https://ws.pagseguro.uol.com.br/v3/transactions/notifications/' + notificationCode + 'token=' + token + '&email=' + email).then(notifXML => {
        parse(notifXML, (error, tr) => {
            const status = tr.transaction.status[0];
            const code = tr.transaction.code[0];
            const amount = tr.transaction.grossAmount[0];
            const campanha = tr.transaction.items[0].item[0].id[0];

            admin.database().ref('/transactions/' + code).set({ transaction }).then(() => { 
                // Não faz nada
             })

            // const campanha = '-L9CI6ezC_iAGcqAuzc4'
            // const amount = '4.00'

            admin.database().ref('/campanhas/' + campanha).once('value').then(v => {
                const campAtual = v.val()

                const doado = parseFloat(campAtual.doado) + parseFloat(amount)
                campAtual.doado = doado.toFixed(2)

                admin.database().ref('/campanhas/' + campanha).set(campAtual).then(() => {
                    res.send('ok')
                })
            })
        })
    })


})

exports.api = functions.https.onRequest(app)
